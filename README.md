# MayhemProject

This is the Mayem project (hope you get the reference...). It is meant to match gitlab interface and terminology to Scrum.

**!! spoiler alert !!**

It doesn't fit perfectly.

To match terminologies perfectly, you have to pay for premium version of gilab or add paying extensions to github.

## User stories -> Gitlab Issues

Usually, people map user stories to gitlab issues. To make things easier:
- you can add markdown templates to issues (I made a 'userstories' one on this project)
- you can add a list of story points to your user story or any TODO to have a completion estimation
- issues can be labeled and labels can be prioritized

## Backlog

The backlog is mapped to the 'issues List' page in gitlab.
- you can search
- you can filter (on anything)
- you can order (label priority, ...)

## Sprint -> Milestone

Usually, people map sprints to milestones. To make things easier:
- you can attach issues to milestones (user stories -> sprint)
- you can add due dates to milestones

## Board -> Board

You can see your project as a board:
- cards are issues
- add as many lists as you want
- put rules to automatically place cards in lists

## Scrum blasphemy
- no place for epics
- board is not that good (particularly when it comes to priorities)
- a lot more that I don't know about...
